Put in your foundry modules folder so the file structure looks like this:

FoundryVTT\Data\modules\jake-swamp-maps\<maps, packs, themes, tiles, etc>

Then launch foundry and activate the module and it should add two compendiums.
One contains premade scenes and the other contains thematic images saved into journal entries.